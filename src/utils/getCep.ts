import { AppError } from '@errors/AppError';
import axios from 'axios';

interface Cep {
  logradouro: string;
  bairro: string;
  localidade: string;
}

export async function getCep(cep: string): Promise<Cep> {
  if (cep.length === 8) {
    try {
      const response = await axios.get<Cep>(
        `https://viacep.com.br/ws/${cep}/json/`,
      );

      if (response.data.localidade) {
        return response.data;
      }

      throw new AppError('CEP Inválido!');
    } catch (error) {
      throw new AppError('CEP Inválido!');
    }
  }

  throw new AppError('O CEP precisa ter exatos 8 dígitos!');
}
