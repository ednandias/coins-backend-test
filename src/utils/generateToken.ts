import { sign } from 'jsonwebtoken';

export function GenerateToken(userId: string): string {
  const token = sign({}, process.env.SECRET_KEY, {
    subject: userId,
    expiresIn: '1d',
  });

  return token;
}
