import { Router } from 'express';
import { AuthMiddleware } from '@middlewares/AuthMiddleware';

import { AuthenticateUserController } from '@modules/users/useCase/authenticateUser/AuthenticateUserController';
import { CreateUserController } from '@modules/users/useCase/createUser/CreateUserController';
import { AlterUserController } from '@modules/users/useCase/alterUser/AlterUserController';
import { UpdateUserController } from '@modules/users/useCase/updateUser/UpdateUserController';
import { GetBalanceController } from '@modules/balance/useCase/getBalance/GetBalanceController';
import { GetCellRechargeController } from '@modules/recharge/useCase/getCellRecharge/GetCellRechargeController';
import { CellRechargeController } from '@modules/recharge/useCase/cellRecharge/CellRechargeController';
import { FindCellRechargeController } from '@modules/recharge/useCase/findCellRecharge/FindCellRechargeController';
import { CreateTransactionController } from '@modules/transactions/useCase/createTransaction/CreateTransactionController';

const router = Router();

const getCellRechargeController = new GetCellRechargeController();
const findCellRechargeController = new FindCellRechargeController();
const cellRechargeController = new CellRechargeController();
const getBalanceController = new GetBalanceController();
const createUserController = new CreateUserController();
const authenticateUserController = new AuthenticateUserController();
const updateUserController = new UpdateUserController();
const alterUserController = new AlterUserController();
const createTransactionController = new CreateTransactionController();

// Balance
router.get('/users/balance', AuthMiddleware, getBalanceController.handle);

// Cell Recharge
router.get(
  '/users/cell_recharge',
  AuthMiddleware,
  getCellRechargeController.handle,
);

router.get('/users/cell_recharge/:id', findCellRechargeController.handle);

router.post(
  '/users/cell_recharge',
  AuthMiddleware,
  cellRechargeController.handle,
);

// Users
router.post('/users', createUserController.handle);

router.put('/users/:id', updateUserController.handle);

router.post('/users/:id/:option', alterUserController.handle);

router.post('/users/authenticate', authenticateUserController.handle);

// Transaction
router.post(
  '/users/transactions',
  AuthMiddleware,
  createTransactionController.handle,
);

export { router };
