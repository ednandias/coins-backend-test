import { AppError } from '@errors/AppError';
import { NextFunction, Request, Response } from 'express';
import { verify } from 'jsonwebtoken';

export const AuthMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction,
): void => {
  const authHeader = req.headers.authorization;

  if (!authHeader) throw new AppError('Token faltando!', 401);

  const parts = authHeader.split(' ');

  if (parts.length !== 2) throw new AppError('Erro de token!', 401);

  const [scheme, token] = parts;

  if (!/^Bearer$/i.test(scheme))
    throw new AppError('Token mal formatado!', 401);

  verify(token, process.env.SECRET_KEY, (err, decoded) => {
    if (err) throw new AppError('Token inválido!', 401);

    if (decoded !== undefined) {
      req.user_id = decoded.sub as string;
    }

    return next();
  });
};
