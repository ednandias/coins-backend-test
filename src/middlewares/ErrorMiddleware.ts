/* eslint-disable no-console */
import { AppError } from '@errors/AppError';
import { Request, Response, NextFunction } from 'express';

export default function ErrorMiddleware(
  err: Error,
  req: Request,
  res: Response,
  _: NextFunction,
): Response {
  if (err instanceof AppError) {
    return res.status(err.code).json({ message: err.message });
  }

  if (process.env.NODE_ENV === 'development') {
    console.log({ error: err });
  }

  return res.status(500).json({ message: 'Internal Server Error' });
}
