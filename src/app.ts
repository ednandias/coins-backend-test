import 'dotenv/config';
import express from 'express';
import 'express-async-errors';
import ErrorMiddleware from '@middlewares/ErrorMiddleware';
import { router } from './routes';

const app = express();

app.use(express.json());
app.use('/api', router);

app.use(ErrorMiddleware);

export { app };
