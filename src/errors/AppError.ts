export class AppError {
  public message: string;

  public code: number;

  constructor(message: string, code = 400) {
    this.code = code;
    this.message = message;
  }
}
