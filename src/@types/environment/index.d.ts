declare namespace NodeJS {
  export interface ProcessEnv {
    PORT: string;
    NODE_ENV: 'production' | 'development';
    SECRET_KEY: string;
  }
}
