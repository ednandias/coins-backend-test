import { prisma } from '@database/prisma';
import { AppError } from '@errors/AppError';

import { Transactions } from '@prisma/client';

interface CreateTransaction {
  user_id: string;
  operation: 'deposit' | 'withdraw';
  amount: number;
}

export class CreateTransactionUseCase {
  async execute({
    user_id,
    operation,
    amount,
  }: CreateTransaction): Promise<Transactions> {
    const user = await prisma.users.findFirst({
      where: { id: user_id },
      include: { balance: true },
    });

    if (!user) {
      throw new AppError('Usuário não encontrado!', 404);
    }

    if (user.balance) {
      if (operation === 'deposit') {
        const amountTotal = user.balance.total + amount;

        await prisma.balance.update({
          where: {
            user_id,
          },
          data: {
            total: amountTotal,
          },
        });
      } else if (operation === 'withdraw') {
        if (amount > user.balance.total || amount === 0) {
          throw new AppError('Valor não disponível para saque!');
        }

        const amountTotal = user.balance.total - amount;

        await prisma.balance.update({
          where: {
            user_id,
          },
          data: {
            total: amountTotal,
          },
        });
      } else {
        throw new AppError(`Apenas 'withdraw' e 'deposit' são permitidos!`);
      }
    }

    const transaction = await prisma.transactions.create({
      data: {
        user_id,
        operation,
        amount,
        balance_id: user.balance?.id,
      },
    });

    return transaction;
  }
}
