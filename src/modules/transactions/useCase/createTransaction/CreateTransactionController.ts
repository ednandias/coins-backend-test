import { Request, Response } from 'express';
import { CreateTransactionUseCase } from './CreateTransactionUseCase';

interface Body {
  amount: number;
  operation: 'deposit' | 'withdraw';
}

export class CreateTransactionController {
  async handle(req: Request, res: Response): Promise<Response> {
    const { amount, operation } = req.body as Body;
    const { user_id } = req;

    const createTransactionUseCase = new CreateTransactionUseCase();

    const transaction = await createTransactionUseCase.execute({
      amount,
      operation,
      user_id,
    });

    return res.json({ transaction });
  }
}
