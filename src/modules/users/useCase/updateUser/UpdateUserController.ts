import { Request, Response } from 'express';
import { UpdateUserUseCase } from './UpdateUserUseCase';

export class UpdateUserController {
  async handle(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const { ...data } = req.body;

    const updateUserUseCase = new UpdateUserUseCase();

    const user = await updateUserUseCase.execute({ id, data });

    return res.json({ user });
  }
}
