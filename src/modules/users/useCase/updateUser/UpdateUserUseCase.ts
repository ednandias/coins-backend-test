import { prisma } from '@database/prisma';
import { AppError } from '@errors/AppError';
import { Users } from '@prisma/client';
import { getCep } from '@utils/getCep';

interface UpdateUser {
  id: string;
  data: {
    name?: string;
    cep?: string;
    street?: string;
    number?: string;
    neighborhood?: string;
    city?: string;
  };
}

export class UpdateUserUseCase {
  async execute({ id, data }: UpdateUser): Promise<Users> {
    let dataFormatted: UpdateUser['data'] = {};

    const userExists = await prisma.users.findFirst({ where: { id } });

    if (!userExists) {
      throw new AppError('Usuário não encontrado!', 404);
    }

    if (data.cep !== '') {
      const address = await getCep(data.cep as string);

      if (data.city !== address.localidade) {
        throw new AppError(
          'As cidade obtida pela API não corresponde a inserida!',
        );
      }

      if (address.logradouro !== '' && address.bairro !== '') {
        dataFormatted = {
          ...dataFormatted,
          neighborhood: address.bairro,
          street: address.logradouro,
        };
      } else if (data.neighborhood !== '' && data.street !== '') {
        dataFormatted = {
          ...dataFormatted,
          neighborhood: data.neighborhood,
          street: data.street,
        };
      }
    }

    if (
      (data.street !== '' || data.city !== '' || data.neighborhood !== '') &&
      data.cep === ''
    ) {
      throw new AppError(
        'Ao preencher rua, cidade, bairro, o CEP também se torna necessário!',
      );
    }

    Object.entries(data).map(e => {
      if (e[1] !== '') {
        const newObject = {
          [e[0]]: e[1],
        };

        dataFormatted = {
          ...dataFormatted,
          ...newObject,
        };

        return dataFormatted;
      }

      return undefined;
    });

    const user = await prisma.users.update({
      where: { id },
      data: {
        ...dataFormatted,
      },
    });

    return user;
  }
}
