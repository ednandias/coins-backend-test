import { prisma } from '@database/prisma';
import { AppError } from '@errors/AppError';
import { GenerateToken } from '@utils/generateToken';
import { compare } from 'bcryptjs';

interface AuthenticateUser {
  email: string;
  password: string;
}

export class AuthenticateUserUseCase {
  async execute({ email, password }: AuthenticateUser): Promise<string> {
    const userExists = await prisma.users.findFirst({ where: { email } });

    if (!userExists) {
      throw new AppError('E-mail ou senha inválidos!');
    }

    if (!userExists.active) {
      throw new AppError('Usuário desativado!');
    }

    const passwordMatch = await compare(password, userExists.password);

    if (!passwordMatch) {
      throw new AppError('E-mail ou senha inválidos!');
    }

    const token = GenerateToken(userExists.id);

    return token;
  }
}
