import { prisma } from '@database/prisma';
import { AppError } from '@errors/AppError';

interface AlterUser {
  id: string;
  option: string;
}

export class AlterUserUseCase {
  async execute({ id, option }: AlterUser): Promise<string> {
    const userExists = await prisma.users.findFirst({ where: { id } });

    if (!userExists) {
      throw new AppError('Usuário não encontrado!', 404);
    }

    if (option === 'enable') {
      await prisma.users.update({
        where: { id },
        data: {
          active: true,
        },
      });

      return 'Usuário ativado com sucesso!';
    }

    if (option === 'disable') {
      await prisma.users.update({
        where: { id },
        data: {
          active: false,
        },
      });

      return 'Usuário Desativado com sucesso!';
    }

    throw new AppError(
      `Apenas as opções 'disable' ou 'enable' são permitidas!`,
    );
  }
}
