import { Request, Response } from 'express';
import { AlterUserUseCase } from './AlterUserUseCase';

export class AlterUserController {
  async handle(req: Request, res: Response): Promise<Response> {
    const { id, option } = req.params;

    const alterUserUseCase = new AlterUserUseCase();

    const message = await alterUserUseCase.execute({ id, option });

    return res.json({ message });
  }
}
