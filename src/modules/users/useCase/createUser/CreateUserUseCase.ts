import { prisma } from '@database/prisma';
import { AppError } from '@errors/AppError';
import { Users } from '@prisma/client';
import { getCep } from '@utils/getCep';
import { hash } from 'bcryptjs';
import * as yup from 'yup';
import { pt } from 'yup-locale-pt';

interface CreateUser {
  data: {
    name: string;
    email: string;
    password: string;
    cep: string;
    street: string;
    number: string;
    neighborhood: string;
    city: string;
  };
}

yup.setLocale(pt);

export class CreateUserUseCase {
  async execute({ data }: CreateUser): Promise<Users> {
    const address = await getCep(data.cep);

    let dataFormatted: CreateUser['data'] = { ...data };

    const schema = yup.object().shape({
      name: yup.string().required(),
      email: yup.string().required().email(),
      password: yup.string().required(),
      cep: yup.string().required(),
      street: yup.string().required(),
      number: yup.string().required(),
      neighborhood: yup.string().required(),
      city: yup.string().required(),
    });

    const userExists = await prisma.users.findFirst({
      where: { email: data.email },
    });

    if (userExists) {
      throw new AppError('E-mail já cadastrado!');
    }

    if (data.city !== address.localidade) {
      throw new AppError(
        'As cidade obtida pela API não corresponde a inserida!',
      );
    }

    if (address.logradouro !== '' && address.bairro !== '') {
      dataFormatted = {
        ...dataFormatted,
        neighborhood: address.bairro,
        street: address.logradouro,
      };
    } else {
      dataFormatted = {
        ...dataFormatted,
        neighborhood: data.neighborhood,
        street: data.street,
      };
    }

    await schema.validate(dataFormatted).catch(e => {
      e.errors.map((err: string) => {
        throw new AppError(err);
      });
    });

    const passwordHashed = await hash(data.password, 8);

    const user = await prisma.users.create({
      data: {
        ...dataFormatted,
        password: passwordHashed,
      },
    });

    await prisma.balance.create({
      data: {
        user_id: user.id,
      },
    });

    return user;
  }
}
