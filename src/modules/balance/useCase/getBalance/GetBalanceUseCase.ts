import { prisma } from '@database/prisma';
import { AppError } from '@errors/AppError';
import { Balance } from '@prisma/client';

export class GetBalanceUseCase {
  async execute(user_id: string): Promise<Balance> {
    const balance = await prisma.balance.findFirst({
      where: {
        user_id,
      },
      include: {
        transactions: {
          orderBy: {
            created_at: 'desc',
          },
        },
      },
    });

    if (!balance) {
      throw new AppError('Não foi possível obter o saldo do usuário!');
    }

    return balance;
  }
}
