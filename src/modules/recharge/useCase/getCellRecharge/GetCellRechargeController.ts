import { Request, Response } from 'express';
import { GetCellRechargeUseCase } from './GetCellRechargeUseCase';

interface QueryRecharge {
  operator?: string;
  amount?: string;
}

export class GetCellRechargeController {
  async handle(req: Request, res: Response): Promise<Response> {
    const { user_id } = req;
    const { operator, amount } = req.query as unknown as QueryRecharge;

    const getCellRechargeUseCase = new GetCellRechargeUseCase();

    const cellRecharge = await getCellRechargeUseCase.execute({
      user_id,
      operator,
      amount,
    });

    return res.json(cellRecharge);
  }
}
