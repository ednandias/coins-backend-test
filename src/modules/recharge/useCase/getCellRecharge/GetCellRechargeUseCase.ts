import { prisma } from '@database/prisma';
import { AppError } from '@errors/AppError';
import { CellRecharge } from '@prisma/client';

interface CellRechargeProps {
  user_id: string;
  operator?: string;
  amount?: string;
}

export class GetCellRechargeUseCase {
  async execute({
    user_id,
    amount,
    operator,
  }: CellRechargeProps): Promise<CellRecharge[]> {
    const user = await prisma.users.findFirst({
      where: {
        id: user_id,
      },
    });

    if (!user) {
      throw new AppError('Usuário não encontrado!', 404);
    }

    if (amount || operator) {
      const cell_recharge = await prisma.cellRecharge.findMany({
        where: {
          OR: [
            {
              operator,
            },
            {
              amount,
            },
          ],
        },
      });

      return cell_recharge;
    }

    const cell_recharge = await prisma.cellRecharge.findMany({
      where: {
        user_id,
      },
    });

    return cell_recharge;
  }
}
