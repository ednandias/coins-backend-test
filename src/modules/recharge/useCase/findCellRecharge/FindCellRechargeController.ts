import { Request, Response } from 'express';
import { FindCellRechargeUseCase } from './FindCellRechargeUseCase';

export class FindCellRechargeController {
  async handle(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;

    const findCellRechargeUseCase = new FindCellRechargeUseCase();

    const cell_recharge = await findCellRechargeUseCase.execute(id);

    return res.json(cell_recharge);
  }
}
