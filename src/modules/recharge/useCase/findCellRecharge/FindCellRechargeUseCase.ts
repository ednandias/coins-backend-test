import { prisma } from '@database/prisma';
import { AppError } from '@errors/AppError';
import { CellRecharge } from '@prisma/client';

export class FindCellRechargeUseCase {
  async execute(id: string): Promise<CellRecharge> {
    const cell_recharge = await prisma.cellRecharge.findFirst({
      where: {
        id,
      },
    });

    if (!cell_recharge) {
      throw new AppError('Recarga não encontrada!', 404);
    }

    return cell_recharge;
  }
}
