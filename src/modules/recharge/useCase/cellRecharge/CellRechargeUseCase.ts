import { prisma } from '@database/prisma';
import { AppError } from '@errors/AppError';
import { CellRecharge } from '@prisma/client';
import { compare } from 'bcryptjs';

interface Recharge {
  user_id: string;
  cell: string;
  amount: string;
  operator: string;
  password: string;
}

export class CellRechargeUseCase {
  async execute({
    user_id,
    cell,
    amount,
    operator,
    password,
  }: Recharge): Promise<CellRecharge> {
    const user = await prisma.users.findFirst({
      where: { id: user_id },
    });

    if (!user) {
      throw new AppError('Usuário não encontrado!', 404);
    }

    const isMatch = await compare(password, user.password);

    if (!isMatch) {
      throw new AppError('Senha incorreta!');
    }

    const recharge = await prisma.cellRecharge.create({
      data: {
        user_id,
        cell,
        amount,
        operator,
      },
    });

    return recharge;
  }
}
