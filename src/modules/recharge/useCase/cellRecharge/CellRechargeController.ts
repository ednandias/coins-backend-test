import { Request, Response } from 'express';
import { CellRechargeUseCase } from './CellRechargeUseCase';

export class CellRechargeController {
  async handle(req: Request, res: Response): Promise<Response> {
    const { user_id } = req;
    const { amount, cell, operator, password } = req.body;

    const cellRechargeUseCase = new CellRechargeUseCase();

    const recharge = await cellRechargeUseCase.execute({
      user_id,
      amount,
      cell,
      operator,
      password,
    });

    return res.json({ recharge });
  }
}
