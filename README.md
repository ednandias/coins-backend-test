# 🚧 Coins Backend Test

Projeto realizado para teste, tomado como base o repositório: **https://gitlab.com/coinswalletbr/coins-backend-test**

# 📢 Como executar?

- Clone o projeto:
  `git clone https://gitlab.com/ednandias/coins-backend-test.git`
  **ou**
  `git clone git@gitlab.com:ednandias/coins-backend-test.git`

- Execute Yarn ou NPM para instalar as dependências:
  `yarn`
  **ou**
  `npm install`

- Renomeie o arquivo **.env.example** para **.env** e altere as seguintes informações:

  - **DATABASE_URL:** Altere as informações **USERNAME**, **PASSWORD** e **DATABASE** para as configurações do seu banco de dados local.

    - ℹ️ URL para o ORM Prisma se conectar com o seu banco de dados local.

  - **SECRET_KEY:** Acesse o site **https://www.md5hashgenerator.com/**, gere a sua chave MD5 e cole no arquivo.
    - ℹ️ Chave única para criação de tokens com **JWT**.

- Rode as migrações do ORM Prisma:
  `yarn prisma migrate dev`

# Insomnia

- **Importando o arquivo json para as requisições**

  - No insomnia, clique **Application**, **Preferences**, **Data**, **Import Data**, **From File**

  _Arquivo pode ser encontrado em insomnia/coins_backend_test.json_

# ℹ️ Sobre

**Realizado por Ednan Dias.**
